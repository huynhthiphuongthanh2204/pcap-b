def new_line():
    print('.')
def three_lines():
    print('.')
    print('.')
    print('.')
    

def nine_lines():
    three_lines()
    three_lines()
    three_lines()
    
        
def clear_screen():
    nine_lines()
    nine_lines()
    three_lines()
    three_lines()
    new_line()
    
l1 = '9 lines successfully printed'
l2 = 'Calling clearScreen() to print 25 lines'
l3 = "25 lines successfully printed"

def main():   
    print(nine_lines(),l1)
    print(l2)
    clear_screen()
    print(l3)
if __name__ == '__main__':
    main()
