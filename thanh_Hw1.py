s= "Today is nice day , so we can go anywhere"
#a) đếm số kí tự
print(len(s))
#b) xuất vị trí -5 đến -9
print(s[-5:-9])
#c) xuất ký tự trong chuỗi với bước nhảy là 3
print(s[None:None:3])
#d)tách chuỗi thành s1,s2
str1=s[0:19]
print("s1 = ",str1)
str2=s[19:]
print("s2 = ",str2)
#e)hợp s1,s2 và sắp xếp theo bảng chữ cái
print(''.join([str1, str2]))
print(sorted(s))
#f)chuyển chuôi thành in hoa
print(s.upper())
